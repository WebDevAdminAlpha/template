# Template

Template project for building new Secure analyzers

## Project Setting TODOs

- [ ] Remove this section of the README once done with the tasks herein
- [ ] Disable Issues as the gitlab project is used for analyzer issues
- [ ] Set a description for the project and make sure that it has `SAST`, `Dependency Scanning`, or `Container Scanning` in the description.
- [ ] Fill out the topics with `GL-Secure`, `GL-Secure Analyzer`, and a group from below

## Group Project Topics

The topics we're using for designating each group are as follows:

- `Container Scanning`
- `Dependency Scanning`
- `SAST`

---

# TODO analyzer

`TODO: (SAST or Dependency Scanning)` for `TODO: (supported language/package manager/framework)` projects. It's based on `TODO: (name+link to scanner)`.

This analyzer is written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## Versioning and release process

Please check the common [Versioning and release process documentation](https://gitlab.com/gitlab-org/security-products/analyzers/common/blob/master/README.md#versioning-and-release-process).

## How to update the upstream Scanner

- Check for the latest version at `TODO: (link to git tags or releases)`
- Compare with the value of `TODO: (name of Env variable)` in the [Dockerfile](./Dockerfile) and update if necessary
- If an update is available, create a branch and bump the version.
- Any extra step (e.g. update a mapping table)
- Trigger a pipeline on all relevant [test projects `TODO:(add relevant topic to this link)`](https://gitlab.com/explore/projects?tag=Secure-QA):
    - trigger a manual pipeline on `master` branch with these variables:
      - TODO: (choose `DS_DEFAULT_ANALYZERS` or `SAST_DEFAULT_ANALYZERS`): "" (just leave the form input blank to send an empty string)
      - TODO: (choose `DS_ANALYZER_IMAGES` or `SAST_ANALYZER_IMAGES`): "_docker_image_tag_for_your_branch_"

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT Expat license, see the [LICENSE](LICENSE) file.
