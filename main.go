package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/template/plugin" // TODO
)

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Usage = "XXX analyzer for GitLab SAST" // TODO
	app.Authors = []*cli.Author{{Name: "GitLab"}}

	app.Commands = command.NewCommands(command.Config{
		Match:        plugin.Match,
		Analyze:      analyze,
		AnalyzeFlags: analyzeFlags(),
		Convert:      convert,
	})

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
