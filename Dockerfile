FROM scratch
COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
